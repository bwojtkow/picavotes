# coding=utf-8

# This code is delivered under the gplv3 license
# It's purpose is to determine automatically the results of all votes in the
# ToutDouxCratie channel of Picasoft's Mattermost instance

# Author : Baptiste Wojtkowski & Kyane Pichou
# contact : baptiste@wojtkowski.fr

"""Get toutdouxcratie stats"""

# Imports
from datetime import datetime
import re
import argparse
from getpass import getpass
from mattermostdriver import Driver

# Constants
ORDINARY_OUTPUT_FILE = "action-ordinaire.md"
EXTRAORDINARY_OUTPUT_FILE = "action-extraordinaire.md"

FOR_EMOJIS = [
    "+1",
    "ok_hand"
]
AGAINST_EMOJIS = [
    "-1"
]
NEUTRAL_EMOJIS = [
    "woman_shrugging",
    "man_shrugging",
    "white_flag",
    "hourglass_flowing_sand",
    "mask"
]

MATTERMOST_DOMAIN = "team.picasoft.net"
MATTERMOST_TEAM = "picasoft"
MATTERMOST_CHANNEL = "toutdouxcratie"
MATTERMOST_FIRST_TIMESTAMP = 1577836800000  # Timestamp of the date where the script will start to get votes


def mattermost_login(username, password):
    """
    Login to mattermost server.
    :param username: username to use
    :param password: password for the user
    :return mm_driver: Mattermost Driver object for the API.
    """
    mm_driver = Driver({
        "url": MATTERMOST_DOMAIN,
        "scheme": "https",
        "port": 443,
        "login_id": username,
        "password": password,
    })
    mm_driver.login()

    return mm_driver


def get_posts(mm_driver):
    """
    Get all posts from the votes channel, sorted by date.
    :param mm_driver: Mattermost Driver object to query API
    :return posts: List of posts objects
    """
    # Get channel ID
    chan_id = mm_driver.channels.get_channel_by_name_and_team_name(MATTERMOST_TEAM, MATTERMOST_CHANNEL)['id']

    # Get all posts from the vote channel
    posts = [post for _, post in mm_driver.posts.get_posts_for_channel(
        chan_id, {"since": MATTERMOST_FIRST_TIMESTAMP})["posts"].items()]

    # Sort posts by creation date
    posts.sort(key=lambda post: post["create_at"])

    return posts


def get_author_name(mm_driver, user_id):
    """
    Get the first + last name of a Mattermost user based on the user ID.
    :param mm_driver: Mattermost Driver object to query API
    :param user_id: Mattermost user ID
    :return user_name: User first name + last name
    """
    user = mm_driver.users.get_user(user_id)
    return user["first_name"] + " " + user["last_name"]


def main():
    """Run the main function."""
    # Create parser for CLI arguments
    parser = argparse.ArgumentParser(description="Get data from Picasoft's toutdouxcratie Mattermost channel.")
    subparsers = parser.add_subparsers(dest='cmd')
    parser.add_argument('--username', help="Username to connect to Mattermost API")
    parser.add_argument('--password', help="Password to connect to Mattermost API")
    parser.add_argument('--debug', action="store_true", help="Show errors messages and ignored votes")
    # Create sub command for stats
    subparsers.add_parser('stats', help='Get votes stats')
    # Create sub command for export
    subparsers.add_parser('export', help='Export reports to markdown files')

    args = parser.parse_args()
    # Check which command to run
    if args.cmd is None:
        command = "stats"
    else:
        command = args.cmd

    # Ask for user and/or password if necessary
    if args.username is None:
        mm_username = input('Enter Mattermost username:')
    else:
        mm_username = args.username
    if args.password is None:
        mm_password = getpass(prompt='Enter Mattermost password for user "{}":'.format(mm_username))
    else:
        mm_password = args.password

    # Connect to Mattermost server
    mm_driver = mattermost_login(mm_username, mm_password)

    # Get all posts from the vote channel
    posts = get_posts(mm_driver)

    # Initialize some counters
    people_count = 0  # Count people in channel
    extraordinary_count = 0  # Count extraordinary votes
    ordinary_count = 0  # Count ordinary votes

    # Write Markdown files headers
    if command == "export":
        with open(EXTRAORDINARY_OUTPUT_FILE, "w") as file_object:
            file_object.write("| ID | Objet | Meneur.se | Date | Pour | Contre | Lien | \n" +
                              "| --- | --------------------------- | ---- | ---- | ---- | ------ | ---- |\n")
        with open(ORDINARY_OUTPUT_FILE, "w") as file_object:
            file_object.write("| ID | Objet | Meneur.se | Date | Pour | Contre | Lien | \n" +
                              "| --- | --------------------------- | ---- | ---- | ---- | ------ | ---- |\n")

    # Loop on posts
    for post in posts:
        # Count people
        if post["type"] == "system_add_to_channel" or post["type"] == "system_join_channel":
            people_count += 1
            continue
        if post["type"] == "system_leave_channel" or post["type"] == "system_remove_from_channel":
            people_count -= 1
            continue

        # Check if it's a vote. Ignore other messages
        if re.match(r".*#action.*", post["message"]) is None and re.match(r".*#decision.*", post["message"]) is None:
            continue
        # Drop messages without reactions
        if "reactions" not in post["metadata"].keys():
            continue

        # Counters for the vote
        for_count = 0
        against_count = 0
        neutral_count = 0
        voters = []

        for reaction in post["metadata"]["reactions"]:
            # If the user already votes, ignore him
            if reaction["user_id"] in voters:
                if args.debug:
                    user_name = get_author_name(mm_driver, reaction['user_id'])
                    print('Ignoring emoji ' + reaction["emoji_name"] +
                          ' from ' + user_name + ' who already votes.')
                continue

            # Count votes
            if reaction["emoji_name"] in FOR_EMOJIS:
                for_count += 1
                voters.append(reaction["user_id"])
                continue
            elif reaction["emoji_name"] in AGAINST_EMOJIS:
                against_count += 1
                voters.append(reaction["user_id"])
                continue
            elif reaction["emoji_name"] in NEUTRAL_EMOJIS:
                neutral_count += 1
                voters.append(reaction["user_id"])
                continue
            else:
                if args.debug:
                    user_name = get_author_name(mm_driver, reaction['user_id'])
                    print('Ignoring emoji ' + reaction["emoji_name"] +
                          ' from ' + user_name + ' which is not a valid vote.')
                continue

        voters_count = len(voters)

        # Process result for differents command
        if command == "stats":
            print(datetime.utcfromtimestamp(post['create_at']/1000).strftime("%d-%m-%Y") + " : " +
                  str(round((voters_count*100)/people_count, 2)) + "% de participation soit " + str(people_count - len(voters)) + " non-participants.")
        elif command == "export":
            # Determine the leader of the vote
            user_name = get_author_name(mm_driver, post["user_id"])

            # Determine if the action is extraordinary and append the new line to the propper file if it is
            if re.match(r".*#action-extraordinaire.*", post["message"]):
                extraordinary_count = extraordinary_count + 1
                line = "| " + " #" + str(extraordinary_count) +  \
                    " | " + post["message"].replace('\n', ' ').replace('\u22c5', ' ') + \
                    " | " + user_name + \
                    " | " + datetime.utcfromtimestamp(post['create_at']/1000).strftime("%d-%m-%Y") + \
                    " | " + str(for_count) + "/" + str(voters_count) + \
                    " | " + str(against_count) + "/" + str(voters_count) + \
                    " | " + "[Lien](" + \
                    "https://" + MATTERMOST_DOMAIN + "/" + MATTERMOST_TEAM + "/pl/" + str(post["id"]) + ") |\n"
                with open(EXTRAORDINARY_OUTPUT_FILE, "a") as file_object:
                    file_object.write(line)

            # Determine if the action is ordinary and append the new line to the propper file if it is
            if re.match(r".*#action-ordinaire.*", post["message"]):
                ordinary_count = ordinary_count + 1
                line = "| " + " #" + str(ordinary_count) +  \
                    " | " + post["message"].replace('\n', ' ').replace('\u22c5', ' ') + \
                    " | " + user_name + \
                    " | " + datetime.utcfromtimestamp(post['create_at']/1000).strftime("%d-%m-%Y") + \
                    " | " + str(for_count) + "/" + str(voters_count) + \
                    " | " + str(against_count) + "/" + str(voters_count) + \
                    " | " + "[Lien](" + \
                    "https://" + MATTERMOST_DOMAIN + "/" + MATTERMOST_TEAM + "/pl/" + str(post["id"]) + ") | \n"
                with open(ORDINARY_OUTPUT_FILE, "a") as file_object:
                    file_object.write(line)


if __name__ == '__main__':
    main()
