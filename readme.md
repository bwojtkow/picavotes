# Picavotes

This code is delivered under the gplv3 license

It's purpose is to get some data about votes in the ToutDouxCratie channel of Picasoft's Mattermost instance.

## Usage

This script is easy to use and require a Mattermost user who can access to the Mattermost channel used by Picasoft for the votes.

### Credentials

Credentials will be asked by the script at runtime :

```bash
$ python3 picavotes.py
Enter Mattermost username:kyane
Enter Mattermost password for user "kyane":
```

or can be passed as CLI arguments :

```bash
$ python3 picavotes.py --username kyane
Enter Mattermost password for user "kyane":
```

```bash
$ python3 picavotes.py --username kyane --password toto
```

### Commands

The script accept differents commands :

- `stats` that print some stats about the votes (default action)
- `export` which generate Markdown reports

You just need to specify the command when running the script :

```bash
$ python3 picavotes.py --username kyane export
```

### Debug

Also, a `--debug` flag allow to print some debug informations about votes or messages that are ignored by the script.

## Contact

Author : Baptiste Wojtkowski & Kyane Pichou
contact : baptiste@wojtkowski.fr
